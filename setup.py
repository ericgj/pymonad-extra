from setuptools import setup

setup(
  name='pymonad-extra',
  version='4.5',
  description='Extra monads and functions for pymonad',
  url='https://bitbucket.com/ericgj/pymonad-extra',
  author='Eric Gjertsen',
  author_email='ericgj72@gmail.com',
  license='MIT',
  packages=['pymonad_extra', 'pymonad_extra/util'],
  dependency_links=[
    'https://bitbucket.org/ericgj/pymonad/get/master.zip#egg=pymonad'
  ],
  install_requires=[
    'pymonad >=1.3, <2.0'
  ]
)

  
