# Validation applicative functor

from pymonad.Applicative import *

class Validation(Applicative):

  def __init__(self,value):
    raise NotImplementedError

  def __eq__(self, other):
    if not isinstance(other, Validation): raise TypeError("Can't compare different types.")

  @classmethod
  def unit(cls, value):
    return Success(value)


class Success(Validation):

  def __init__(self, value):
    self.value = value

  def __eq__(self, other):
    super(Success,self).__eq__(other)
    if not isinstance(other, Success): return False
    elif (self.getValue() == other.getValue()): return True
    else: return False

  def __ne__(self, other):
    return not self.__eq__(other)

  def __str__(self):
    return "Success"

  def fmap(self, function):
    return Success(function(self.value))

  def amap(self, functorValue):
    if isinstance(functorValue,Failure):
      return functorValue
    else:
      return functorValue.fmap(self.value)


class Failure(Validation):
  
  def __init__(self, value):
    self.value = value

  def __eq__(self, other):
    super(Failure,self).__eq__(other)
    if not isinstance(other, Failure): return False
    elif (self.getValue() == other.getValue()): return True
    else: return False

  def __ne__(self, other):
    return not self.__eq__(other)

  def __str__(self):
    return "Failure: " + str(self.getValue())

  def fmap(self, function):
    return self

  def amap(self, functorValue):
    if isinstance(functorValue,Failure):
      return Failure(self.value + functorValue.value)
    else:
      return self



