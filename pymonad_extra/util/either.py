from functools import reduce
from pymonad.Either import Left, Right
from pymonad.Maybe import Nothing, Just
from pymonad_extra.Validation import Failure, Success
from pymonad_extra.Task import Task
from .f import curry, always, identity

@curry
def with_default(default,either):
  if isinstance(either,Left):
    return default
  elif isinstance(either,Right):
    return either.value
  else:
    raise TypeError("Not an Either (Left or Right)")
    
@curry
def left_map(fn, either):
  if isinstance(either,Left):
    return Left(fn(either.value))
  elif isinstance(either,Right):
    return either
  else:
    raise TypeError("Not an Either (Left or Right)")

@curry
def fold(ifleft, ifright, either):
  if isinstance(either,Left):
    return ifleft(either.value)
  elif isinstance(either,Right):
    return ifright(either.value)
  else:
    raise TypeError("Not an Either (Left or Right)")

@curry
def filter_map(fn, eithers):
  def _filter(acc,e):
    if isinstance(e,Left):
      return acc
    elif isinstance(e,Right):
      return acc + [ fold(always(None), identity, e) ]
    else:
      raise TypeError("Not an Either (Left or Right)")

  return reduce( _filter, [e.fmap(fn) for e in eithers], [])

def right_of(eithers):
  return filter_map(identity, eithers)

def sequence(eithers):
  def _acc(acc,e):
    return acc >> (lambda vs:  e.fmap(lambda v: vs + [v]) )
  return reduce( _acc, eithers, Right([]) )


def to_maybe(either):
  return with_default(Nothing, either.fmap(Just)) 
  
def to_validation(either):
  return fold(Failure, Success, either)

def to_task(either):
  if isinstance(either,Left):
    return Task( lambda rej,_: rej(either.value) )
  elif isinstance(either,Right):
    return Task( lambda _,res: res(either.value) )
  else:
    raise TypeError("Not an Either (Left or Right)")

