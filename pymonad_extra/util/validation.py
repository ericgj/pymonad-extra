from pymonad_extra.Validation import Failure, Success
from pymonad.Either import Left, Right
from .f import curry, identity

@curry
def fold(iffail, ifsuccess, val):
  if isinstance(val,Failure):
    return iffail(val.value)
  elif isinstance(val,Success):
    return ifsuccess(val.value)
  else:
    raise TypeError("Not a Validation (Success or Failure)")

@curry
def concat(a,b):
  if isinstance(a,Failure):
    if isinstance(b,Failure):
      return Failure( a.value + b.value )
    elif isinstance(b,Success):
      return a
    else:
      raise TypeError("Not a Validation (Success or Failure)")
  elif isinstance(a,Success):
    return b
  else:
    raise TypeError("Not a Validation (Success or Failure)")

def swap(val):
  return fold(Success, Failure, val)

@curry
def bimap(iffail, ifsuccess, val):
  if isinstance(val,Failure):
    return Failure(iffail(val.value))
  elif isinstance(val,Success):
    return Success(ifsuccess(val.value))
  else:
    raise TypeError("Not a Validation (Success or Failure)")

@curry
def failure_map(iffail, val):
  return bimap(iffail, identity, val)


def to_either(val):
  return fold( Left, Right, val )


