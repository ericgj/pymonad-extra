
from functools import reduce
from pymonad.Maybe import Nothing, Just
from pymonad.Either import Left, Right
from pymonad_extra.Validation import Failure, Success
from pymonad_extra.Task import Task
from .f import curry, identity

@curry
def with_default(val,maybe):
  if isinstance(maybe,Just): 
    return maybe.getValue() 
  elif maybe == Nothing:
    return val
  else:
    raise TypeError("Not a Maybe")

@curry
def filter_map(fn, maybes):
  def _filter(acc,m):
    if m == Nothing:
      return acc
    elif isinstance(m,Just):
      return acc + [ with_default(None, m) ]
    else:
      raise TypeError("Not a Maybe")

  return reduce( _filter, [m.fmap(fn) for m in maybes], [])

def just_of(maybes):
  return filter_map(identity, maybes)

def sequence(maybes):
  def _acc(acc,m):
    return acc >> (lambda vs: m.fmap(lambda v: vs + [v]) )
  return reduce( _acc, maybes, Just([]) )


@curry
def to_either(e,maybe):
  return with_default( Left(e), maybe.fmap(Right) )

@curry
def to_validation(e,maybe):
  return with_default( Failure(e), maybe.fmap(Success) )

@curry
def to_task(e,maybe):
  return with_default(
    Task( lambda rej,_: rej(e) ),
    maybe.fmap( lambda x: Task( lambda _,res: res(x) ) )
  )

